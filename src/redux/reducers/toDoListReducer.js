import { arrTheme } from "../../JSS_StyledComponent/Themes/ThemeManagement";
import { ToDoListDarkTheme } from "../../JSS_StyledComponent/Themes/ToDoListDarkTheme"
import { add_task, change_theme, delete_task, done_task, edit_task, update_task } from "../actions/types/ToDoListTypes"

const initialState = {
    themeToDoList: ToDoListDarkTheme,
    taskList: [
        {id: 'task-1', taskName: "Task 1", done: true,},
        {id: 'task-2', taskName: "Task 2", done: false,},
        {id: 'task-3', taskName: "Task 3", done: true,},
        {id: 'task-4', taskName: "Task 4", done: false,},
    ],
    taskEdit: {id: '-1', taskName: "", done: false,},
}

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
        // Kiểm tra trống
        if (action.newTask.taskName.trim() === '') {
            alert("Task name is required!");
            return {...state}
        }

        // Kiểm tra tồn tại
        let taskListUpdate = [...state.taskList];
        let index = taskListUpdate.findIndex(task => task.taskName === action.newTask.taskName);
        if (index !== -1) {
            alert('Task name already exists!');
            return {...state};
        }
        taskListUpdate.push(action.newTask);

        // Xử lý xong thì gán taskList mới vào taskList
        return {...state, taskList: taskListUpdate};
    }
    case change_theme: {
        let themeSelected = arrTheme.find(theme => theme.id == action.themeId)
        if (themeSelected) {
            state.themeToDoList = {...themeSelected.theme};
        }
        return {...state};
    }
    case done_task: {
        let taskListUpdate = [...state.taskList];
        let index = taskListUpdate.findIndex(task => task.id == action.taskId);
        if (index != -1) {
            taskListUpdate[index].done = true;
        }
        return {...state, taskList: taskListUpdate};
    }
    case delete_task: {
        let taskListUpdate = [...state.taskList];
        taskListUpdate = taskListUpdate.filter(task => task.id != action.taskId);
        return {...state, taskList: taskListUpdate};
    }
    case edit_task: {
        return {...state, taskEdit: action.task}
    }
    case update_task: {
        state.taskEdit = {...state.taskEdit, taskName: action.taskName};
        let taskListUpdate = [...state.taskList];
        let index = taskListUpdate.findIndex(task => task.id == state.taskEdit.id);
        if (index != -1) {
            taskListUpdate[index] = state.taskEdit;
        }
        state.taskEdit = {id: '-1', taskName: '', done: false};
        return {...state, taskList: taskListUpdate};
    }
    default:
        return {...state}
    }
}
