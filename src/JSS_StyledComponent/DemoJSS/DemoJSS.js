import React, { Component } from 'react'
import { Button, SmallButton } from '../Components/Button'
import { Link, StyleLink } from '../Components/Link'
import { TextField } from '../Components/TextField'

export default class DemoJSS extends Component {
  render() {
    return (
      <div >
        <Button className="bt" primary>Hello</Button>
        <SmallButton>Hello Tina</SmallButton>
        <StyleLink>ahihi <p>eafe</p></StyleLink>
        <Link>hello </Link>
        <TextField inputColor="green"/>
      </div>
    )
  }
}
