import React, { Component } from 'react'
import { ThemeProvider } from 'styled-components'
import { Container } from '../../ComponentsToDoList/Container'
import {ToDoListDarkTheme} from '../../Themes/ToDoListDarkTheme'
import {ToDoListLightTheme} from '../../Themes/ToDoListLightTheme'
import {ToDoListPrimaryTheme} from '../../Themes/ToDoListPrimaryTheme'
import {Dropdown} from '../../ComponentsToDoList/Dropdown'
import {Heading1, Heading2, Heading3, Heading4, Heading5} from '../../ComponentsToDoList/Heading'
import {TextField, Label, Input} from '../../ComponentsToDoList/TextField'
import {Button} from '../../ComponentsToDoList/Button'
import {Table, Tr, Td, Th, Thead, Tbody} from '../../ComponentsToDoList/Table'
import { connect } from 'react-redux'
import { addTaskAction, changeTheme, deleteTaskAction, doneTaskAction, editTaskAction, updateTaskAction } from '../../../redux/actions/ToDoListActions'
import { arrTheme } from '../../Themes/ThemeManagement'
import { change_theme } from '../../../redux/actions/types/ToDoListTypes'

class ToDoList extends Component {
    state = {
        taskName: '',
        disabled: true,
        // disabledAdd: true,
    }
  renderTheme = () => {
    return arrTheme.map((theme, index) => {
        return <option value={theme.id} key={index}>{theme.name}</option>
    })
  }
  renderTaskToDo = () => {
    return this.props.taskList.filter(task => !task.done).map((task, index) => {
        return (
            <Tr key={index}>
                <Th style={{verticalAlign: 'middle'}}>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {
                        this.setState({disabled: false}, () => {this.props.dispatch(editTaskAction(task))})                        
                        }} className='ml-1'><i className='fa fa-edit'></i></Button>
                    <Button onClick={() => {this.props.dispatch(doneTaskAction(task.id))}} className='ml-1'><i className='fa fa-check'></i></Button>
                    <Button onClick={() => {this.props.dispatch(deleteTaskAction(task.id))}} className='ml-1'><i className='fa fa-trash'></i></Button>
                </Th>
            </Tr>
                        
        )
    })
  }
  renderTaskComplete = () => {
    return this.props.taskList.filter(task => task.done).map((task, index) => {
        return (
            <Tr key={index}>
                <Th style={{verticalAlign: 'middle'}}>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {this.props.dispatch(deleteTaskAction(task.id))}} className='ml-1'><i className='fa fa-trash'></i></Button>
                </Th>
            </Tr>
                        
        )
    })
  }
  
  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.themeToDoList}>
            <Container className='w-50'>
                <Dropdown onChange={(e) => {
                    let {value} = e.target;
                    this.props.dispatch(changeTheme(value))
                }}>{this.renderTheme()}</Dropdown>

                <Heading3>To do list</Heading3>
                <TextField value={this.state.taskName} onChange={(e) => {
                    this.setState({
                        taskName: e.target.value,
                    })
                }} name="taskName" label="Task name" className="w-50"/>
                <Button onClick={() => {
                    let {taskName} = this.state;
                    let newTask = {
                        id:Date.now(),
                        taskName: taskName,
                        done: false,
                    }
                    this.props.dispatch(addTaskAction(newTask))


                }} className='ml-2'><i className='fa fa-plus'></i> Add task</Button>
                {
                    this.state.disabled ? <Button disabled onClick={() => {this.props.dispatch(updateTaskAction(this.state.taskName))}} className='ml-2'><i className='fa fa-upload'></i> Update task</Button> : 
                    <Button onClick={() => {
                        let {taskName} = this.state;
                        this.setState(
                            {disabled: true,
                             taskName: '',
                            },
                            () => {this.props.dispatch(updateTaskAction(taskName))}
                        )}} 
                        className='ml-2'><i className='fa fa-upload'></i> Update task</Button>
                }
                <hr />

                <Heading3>Task to do</Heading3>
                <Table>
                    <Thead>
                        {this.renderTaskToDo()}
                    </Thead>
                </Table>

                <Heading3>Task complete</Heading3>
                <Table>
                    <Thead>
                        {this.renderTaskComplete()}
                    </Thead>
                </Table>
            </Container>
        </ThemeProvider>
        
      </div>
    )
  }

  componentDidUpdate (preProps, preState) {
    if (preProps.taskEdit.id != this.props.taskEdit.id) {
        this.setState({
            taskName: this.props.taskEdit.taskName
        })
    }
  }
}

const mapStateToProps = (state) => {
    return {
        themeToDoList: state.toDoListReducer.themeToDoList,
        taskList: state.toDoListReducer.taskList,
        taskEdit: state.toDoListReducer.taskEdit,
    }
}

export default connect(mapStateToProps)(ToDoList);